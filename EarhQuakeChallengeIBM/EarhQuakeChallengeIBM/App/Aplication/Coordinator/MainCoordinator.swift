//
//  MainCoordinator.swift
//  EarhQuakeChallengeIBM
//
//  Created by FELIPE on 6/11/23.
//

import Foundation
import UIKit

class MainCoordinator : Coordinator {
    
    var rootViewController: UINavigationController
    
    var childs: [Coordinator] = []
    
    //MARK: - es inyectada y indica si se hizo login
    var isUserLoggin : Bool
    
    init(rootViewController: UINavigationController, isUserLoggin: Bool) {
        self.rootViewController = rootViewController
        self.isUserLoggin = isUserLoggin
    }
    
    func start() {
        if isUserLoggin {
            showEarthquakesList()
        }else{
            showUserLogin()
        }
    }
    
    //MARK: - crea y ejecuta MainEarthquakeListCoordinator y start
    func showEarthquakesList(){
        let mainEarthquakeListCoordinator = MainEarthquakeListCoordinator(
            rootViewController: rootViewController,mainCoordinator: self
        )
        mainEarthquakeListCoordinator.start()
        childs.append(mainEarthquakeListCoordinator)
    }
    
    //MARK: - crea y ejecuta UserLoginCoordinator y start
    func showUserLogin(){
        let userLoginCoordinator = UserLoginCoordinator(
            rootViewController: rootViewController, mainCoordinator: self
        )
        userLoginCoordinator.start()
        childs.append(userLoginCoordinator)
    }
    
    //MARK: - muestra login
    func userTappedLogout(){
        let defaults = UserDefaults.standard
        defaults.set(false, forKey: K.Defaults.userDidLogin)
        showUserLogin()
    }
    
}


