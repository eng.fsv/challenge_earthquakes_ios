//
//  Coordinator.swift
//  EarhQuakeChallengeIBM
//
//  Created by FELIPE on 6/11/23.
//

import Foundation
import UIKit

//MARK: - coordinator
protocol Coordinator {
    
    var rootViewController: UINavigationController {get}
    
    var childs: [Coordinator] {get set}
    
    func start()
}
