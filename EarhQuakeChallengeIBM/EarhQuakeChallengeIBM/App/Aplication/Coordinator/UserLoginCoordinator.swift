//
//  UserLoginCoordinator.swift
//  EarhQuakeChallengeIBM
//
//  Created by FELIPE on 6/11/23.
//

import Foundation
import UIKit

class UserLoginCoordinator : Coordinator {
    
    var rootViewController: UINavigationController
    
    var childs: [Coordinator] = []
    
    weak var mainCoordinator : MainCoordinator?

    init(rootViewController: UINavigationController, mainCoordinator: MainCoordinator) {
        self.rootViewController = rootViewController
        self.mainCoordinator = mainCoordinator
    }
    
    //MARK: - muestra login
    func start() {
        let userLoginViewController = UserLoginViewController()
        userLoginViewController.coordinator = self
        rootViewController.setViewControllers([userLoginViewController], animated: true)
    }

    //MARK: - muestra modal de registro
    func showRegisterModal(){
        let userRegisterViewController = UserRegisterViewController()
        rootViewController.present(userRegisterViewController, animated: true)
    }
    
    //MARK: - es llamado en login satisfactorio, setea default en true
    func succesfullLogin(){
        let defaults = UserDefaults.standard
        defaults.set(true, forKey: K.Defaults.userDidLogin)
        mainCoordinator?.showEarthquakesList()
    }
    
}
