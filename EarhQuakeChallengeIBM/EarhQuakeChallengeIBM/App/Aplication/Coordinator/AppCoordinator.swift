//
//  AppCoordinator.swift
//  EarhQuakeChallengeIBM
//
//  Created by FELIPE on 6/11/23.
//

import Foundation
import UIKit

class AppCoordinator : Coordinator {
    
    var rootViewController: UINavigationController
    
    var childs : [Coordinator] = []
    
    init(rootViewController: UINavigationController) {
        self.rootViewController = rootViewController
    }
    
    //MARK: - crea y ejecuta MainCoordinator y start
    func start() {
        let mainCoordinator = MainCoordinator(
            rootViewController: rootViewController, isUserLoggin: validateUserLogged()
        )
        mainCoordinator.start()
        childs.append(mainCoordinator)
    }
    
    //MARK: - lectura en defaults para validad si user inicio sesion
    private func validateUserLogged() -> Bool{
        let defaults = UserDefaults.standard
        let userDidLogin = defaults.bool(forKey: K.Defaults.userDidLogin)
        return userDidLogin
    }
    
}
