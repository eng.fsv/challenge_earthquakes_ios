//
//  NextCoordinator.swift
//  EarhQuakeChallengeIBM
//
//  Created by FELIPE on 6/11/23.
//

import Foundation
import UIKit

class MainEarthquakeListCoordinator : Coordinator {
    
    var rootViewController: UINavigationController
    
    var childs: [Coordinator] = []
    
    weak var mainCoordinator : MainCoordinator?
    
    init(rootViewController: UINavigationController, mainCoordinator: MainCoordinator) {
        self.rootViewController = rootViewController
        self.mainCoordinator = mainCoordinator
    }
    
    //MARK: - Muestra la lista de terremotos
    func start() {
        let earthquakesListViewController = EarthquakesListViewController()
        earthquakesListViewController.coordinator = self
        rootViewController.pushViewController(earthquakesListViewController, animated: true)
        //rootViewController.setViewControllers([earthquakesListViewController], animated: true)
    }
    
    //MARK: - muestra details de lista
    func showDetailsForItem(feature: FeaturesModel){
        let earthquakeDetailViewController = EarthquakeDetailViewController(feature: feature)
        rootViewController.pushViewController(earthquakeDetailViewController, animated: true)
    }
    
    //MARK: - ejecuta metodo de mainCoordinator
    func userTappedLogout(){
        mainCoordinator?.userTappedLogout()
    }
    
}
