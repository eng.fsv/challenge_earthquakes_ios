//
//  UserPersistentDataErrors.swift
//  EarhQuakeChallengeIBM
//
//  Created by FELIPE on 6/11/23.
//

import Foundation
import UIKit

enum UserPersistentDataErrors : Error {
    //MARK: - Escritura
    case errorCodingData
    case duplicatedEntry
    case unknow(OSStatus)
    
    //MARK: - Lectura
    case noItemFound
    case errorObtainData
    case errorDecodingData
}

extension UserPersistentDataErrors: LocalizedError {
    
    public var localizedDescriptionStorageError : String {
        
        switch self {
            
        case .duplicatedEntry:
            return NSLocalizedString("Error al guardar los Datos, ya existe una cuenta registrada con el mismo correo.",
                                     comment: "")
            
        case .unknow(let OSStatus):
            return NSLocalizedString("Error al procesar o guardar los datos: \(OSStatus)",
                                     comment: "")
            
        case .errorCodingData:
            return NSLocalizedString("Error al Codificar la informacion.",
                                     comment: "")
            
        case .noItemFound:
            return NSLocalizedString("No se encontro el usuario, prueba con otro correo o crea una cuenta",
                                     comment: "")
            
        case .errorObtainData:
            return NSLocalizedString("Error al obtener la informacion.",
                                     comment: "")
            
        case .errorDecodingData:
            return NSLocalizedString("Error al Decodificar la informacion.",
                                     comment: "")

        }
        
    }

}

