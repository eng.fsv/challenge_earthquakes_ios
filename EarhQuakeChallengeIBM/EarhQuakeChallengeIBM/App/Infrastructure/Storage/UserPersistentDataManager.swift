//
//  UserPersistentDataManager.swift
//  EarhQuakeChallengeIBM
//
//  Created by FELIPE on 6/11/23.
//

import Foundation
import UIKit

class UserPersistentDataManager {
    
    //MARK: - escritura en keychain
    func saveToKeyChains(model: UserRegisterModel) throws {
      
        do{
            
            let encoder = JSONEncoder()
            let data = try encoder.encode(model)
            
            let query : [String : Any] = [
                kSecClass as String         : kSecClassGenericPassword,
                kSecAttrService as String   : "com.earthQuakeChallengeIBM.register",
                kSecAttrAccount as String     : "\(model.email)",
                kSecValueData as String     : data
            ]
            
            let status = SecItemAdd(query as CFDictionary, nil)
            guard status != errSecDuplicateItem else { throw UserPersistentDataErrors.duplicatedEntry}
            guard status == errSecSuccess else {throw UserPersistentDataErrors.unknow(status)}
            
            
        }catch let error as UserPersistentDataErrors {
            throw error
            
        }catch {
                throw UserPersistentDataErrors.unknow(0)
        }

    }
    
    
    //MARK: - lectura en keychain
    func readFromKeyChains(email : String) throws -> UserRegisterModel? {
        
        let query : [String : Any] = [
            kSecClass as String         : kSecClassGenericPassword,
            kSecAttrService as String   : "com.earthQuakeChallengeIBM.register",
            kSecAttrAccount as String     : "\(email)",
            kSecReturnData  as String   : kCFBooleanTrue!,
            kSecMatchLimit as String    : kSecMatchLimitOne
        ]
        
        var resultRef : AnyObject?
        let status = SecItemCopyMatching(query as CFDictionary, &resultRef )
        
        guard status != errSecItemNotFound else { throw UserPersistentDataErrors.noItemFound }
        guard status == errSecSuccess else { throw UserPersistentDataErrors.unknow(status) }
        guard let data = resultRef as? Data else {throw UserPersistentDataErrors.errorObtainData}
        
        do{
            
            let user = try JSONDecoder().decode(UserRegisterModel.self, from: data)
            return user
            
        }catch let error as UserPersistentDataErrors {
            throw error
            
        }catch {
            throw UserPersistentDataErrors.unknow(0)
        }
        
    }

}

