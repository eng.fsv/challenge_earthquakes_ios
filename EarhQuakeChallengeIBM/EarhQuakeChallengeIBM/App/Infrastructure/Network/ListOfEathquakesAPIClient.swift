//
//  ListOfEathquakesAPIClient.swift
//  EarhQuakeChallengeIBM
//
//  Created by FELIPE on 6/11/23.
//

import Foundation
import UIKit

class ListOfEathquakesAPIClient {
    
    //MARK: - validacion url
    func getListOfEarthquakes(urlComponents: URLComponents ) async throws -> EarthquakeModel {
        guard let url = urlComponents.url else {throw ListOfEathquakesAPIError.invalidURL}
        return try await makeRequest(url: url)
    }
    
    //MARK: - peticion
    private func makeRequest(url: URL) async throws -> EarthquakeModel {
        let request = try await URLSession.shared.data(from: url)
        return try validateRequest(request : request)
    }
    
    //MARK: - validacion request
    private func validateRequest(request: (data: Data, httpResponse: URLResponse) ) throws -> EarthquakeModel {
            
        guard let response = request.httpResponse as? HTTPURLResponse  else {
            throw ListOfEathquakesAPIError.unknown(0)
        }
       
        switch response.statusCode {
            
        case 200:
            return try decode(data: request.data)
            
        case 204:
            throw ListOfEathquakesAPIError.response204
            
        case 400:
            throw ListOfEathquakesAPIError.response400
            
        case 401:
            throw ListOfEathquakesAPIError.response401
            
        case 403:
            throw ListOfEathquakesAPIError.response403
            
        case 413:
            throw ListOfEathquakesAPIError.response413
            
        case 414:
            throw ListOfEathquakesAPIError.response414
            
        case 500:
            throw ListOfEathquakesAPIError.response500
            
        case 503:
            throw ListOfEathquakesAPIError.response503
            
        default:
            throw ListOfEathquakesAPIError.unknown(response.statusCode)
            
        }
    }
    
    //MARK: - decodificacion data
    private func decode(data: Data) throws -> EarthquakeModel {
        guard let model = try? JSONDecoder().decode(EarthquakeModel.self, from: data) else {
            throw ListOfEathquakesAPIError.invalidData
        }
        
        return model
    }
    
}
