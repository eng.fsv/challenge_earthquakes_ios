//
//  ListOfEathquakesAPIError.swift
//  EarhQuakeChallengeIBM
//
//  Created by FELIPE on 6/11/23.
//

import Foundation
import UIKit

enum ListOfEathquakesAPIError : Error {
    
    case invalidURL
    case invalidData
    
    //MARK: - apierrors
    case response204
    case response400
    case response401
    case response403
    case response413
    case response414
    case response500
    case response503
    case unknown(Int)
}



extension ListOfEathquakesAPIError : LocalizedError {
    
    public var localizedDescriptionAPIError : String {
        
        switch self {
            
        case .invalidURL:
            return NSLocalizedString(
                "El URL es invalido",
                comment: ""
            )
            
        case .invalidData:
            return NSLocalizedString(
                "El procesamiento de los datos de la peticion termino en error",
                comment: ""
            )
            
        case .response204:
            return NSLocalizedString(
                "La solicitud fue formateada y enviada correctamente pero ningún dato coincide con la selección",
                comment: ""
            )
            
        case .response400:
            return NSLocalizedString(
                "Solicitud incorrecta debido a una especificación incorrecta, parámetro no reconocido, valor del parámetro fuera de rango",
                comment: ""
            )
            
        case .response401:
            return NSLocalizedString(
                "No autorizado, se requiere autenticación",
                comment: ""
            )
            
        case .response403:
            return NSLocalizedString(
                "Error de autenticación o acceso bloqueado a datos restringidos"
                , comment: ""
            )
            
        case .response413:
            return NSLocalizedString(
                "La solicitud provocaría que se devolvieran demasiados datos",
                comment: ""
            )
            
        case .response414:
            return NSLocalizedString(
                "Solicitar URI demasiado grande",
                comment: ""
            )
            
        case .response500:
            return NSLocalizedString(
                "Error interno de servidor",
                comment: ""
            )
            
        case .response503:
            return NSLocalizedString(
                "Servicio no disponible temporalmente, utilizado en mantenimiento y condiciones de error",
                comment: ""
            )
            
        case .unknown(let status):
            return NSLocalizedString(
                "Error desconocido \(status)",
                comment: ""
            )
            
        }
    }
}
