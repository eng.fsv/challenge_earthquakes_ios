//
//  MapPointModel.swift
//  EarhQuakeChallengeIBM
//
//  Created by FELIPE on 6/11/23.
//

import Foundation
import MapKit

struct MapPointModel {
    let annotations : MKPointAnnotation
    let region : MKCoordinateRegion
}
