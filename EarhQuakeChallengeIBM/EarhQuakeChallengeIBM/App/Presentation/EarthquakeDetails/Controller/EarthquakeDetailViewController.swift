//
//  EarthquakeDetailViewController.swift
//  EarhQuakeChallengeIBM
//
//  Created by FELIPE on 6/11/23.
//

import Foundation
import UIKit
import MapKit

class EarthquakeDetailViewController: UIViewController {
    
    var earthquakeDetailView : EarthquakeDetailView!
    
    //MARK: - recibe Feature inyectada
    var feature : FeaturesModel

    init(feature: FeaturesModel){
        self.feature = feature
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        //MARK: - crea EarthquakeDetailView y asigna feature
        earthquakeDetailView = EarthquakeDetailView()
        earthquakeDetailView.setupEarthquakeDetails(feature: feature)

        view = earthquakeDetailView
 
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Detalles"
        setupMap(feature: feature)
    }
    
    
    //MARK: - setting earthquakeDetailView
    private func setupMap(feature: FeaturesModel) {
        
        let lat         =  feature.geometry.coordinates[1]
        let lon         =  feature.geometry.coordinates[0]
        let title       =  feature.properties.place
        let magnitude   =  feature.properties.mag
        
        let location    = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        let space       = MKCoordinateSpan(latitudeDelta: 10, longitudeDelta: 10)
        let region      = MKCoordinateRegion(center: location, span: space)
   
        let annotations         = MKPointAnnotation()
        annotations.coordinate  = location
        annotations.title = title
        annotations.subtitle    = "magnitud: \(magnitude)"
        
        earthquakeDetailView.setMapRegion(region)
        earthquakeDetailView.setMapAnnotations(annotations)
    }

      
}


