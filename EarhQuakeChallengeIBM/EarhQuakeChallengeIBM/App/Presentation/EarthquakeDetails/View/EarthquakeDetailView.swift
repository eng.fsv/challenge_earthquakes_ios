//
//  EarthquakeDetailView.swift
//  EarhQuakeChallengeIBM
//
//  Created by FELIPE on 6/11/23.
//

import Foundation

import UIKit
import MapKit

class EarthquakeDetailView: UIView {
    
    //MARK: - views
    
    let earthquakePlace : UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.label
        label.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        label.textAlignment = .center
        return label
    }()
    
    let earthquakeTitle : UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.label
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 16, weight: .light)
        return label
    }()
    

    
    //MARK: - top card
    
    let stackInformation : UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fillProportionally
        stack.alignment = .center
        stack.spacing = 10
        stack.backgroundColor = UIColor.systemBackground
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.layoutMargins = UIEdgeInsets(top: 15, left: 10, bottom: 15, right: 10)
        stack.isLayoutMarginsRelativeArrangement = true
        return stack
        
    }()
    
    
    //MARK: - bottom card
    
    let stackMagnitudes : UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fillEqually
        stack.alignment = .bottom
        stack.backgroundColor = UIColor.systemBackground
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.backgroundColor = UIColor(named: K.Colors.bottomMapTab)
        return stack
        
    }()
    
    let magnitudeCell : UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fill
        stack.alignment = .center
        stack.spacing = 5
        stack.layoutMargins = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        stack.isLayoutMarginsRelativeArrangement = true
        return stack
    }()
    

  
    let magnitudeIcon : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(systemName: "waveform.path")
        imageView.tintColor = UIColor(named: K.Colors.bottomIconTab)
        imageView.tintAdjustmentMode = .normal
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let magnitudeLabel : UILabel = {
        let label = UILabel()
        label.textColor = UIColor.label
        label.text = "Magnitud"
        label.font = UIFont.systemFont(ofSize: 16, weight: .light)
        return label
    }()
    
    let earthquakeMagnitude : UILabel = {
        let label = UILabel()
        label.textColor = UIColor.label
        label.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        return label
    }()
    
    
    let depthCell : UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fill
        stack.alignment = .center
        stack.spacing = 5
        stack.layoutMargins = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        stack.isLayoutMarginsRelativeArrangement = true
        return stack
    }()
    
    let depthIcon : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(systemName: "water.waves.and.arrow.down")
        imageView.tintColor = UIColor(named: K.Colors.bottomIconTab)
        imageView.tintAdjustmentMode = .normal
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let depthLabel : UILabel = {
        let label = UILabel()
        label.textColor = UIColor.label
        label.text = "Profundidad"
        label.font = UIFont.systemFont(ofSize: 16, weight: .light)
        return label
    }()
    
    
    let earthquakeDepth : UILabel = {
        let label = UILabel()
        label.textColor = UIColor.label
        label.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        return label
    }()
    
    
    //MARK: - inits
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var regionToSetMap : MKCoordinateRegion!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
       
    }
    
    //MARK: - setup mapview
    
    var mapPointEarthquake : MKMapView = {
        var map = MKMapView()
        map.translatesAutoresizingMaskIntoConstraints = false
        map.backgroundColor = UIColor.systemBackground
        map.showsBuildings = false
        map.showsTraffic = false
        return map
    }()

    
    //MARK: - setups
    
    func setupView(){

        
        backgroundColor = UIColor.systemBackground
        
        stackInformation.addArrangedSubview(earthquakePlace)
        stackInformation.addArrangedSubview(earthquakeTitle)
        
        magnitudeCell.addArrangedSubview(magnitudeIcon)
        magnitudeCell.addArrangedSubview(magnitudeLabel)
        magnitudeCell.addArrangedSubview(earthquakeMagnitude)
        
        depthCell.addArrangedSubview(depthIcon)
        depthCell.addArrangedSubview(depthLabel)
        depthCell.addArrangedSubview(earthquakeDepth)
        
        stackMagnitudes.addArrangedSubview(magnitudeCell)
        stackMagnitudes.addArrangedSubview(depthCell)
        
        addSubview(stackInformation)
        
        addSubview(stackMagnitudes)
        
        addSubview(mapPointEarthquake)
        

        NSLayoutConstraint.activate([
            
            stackInformation.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor, constant: 0),
            stackInformation.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            stackInformation.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            
            stackMagnitudes.bottomAnchor.constraint(equalTo: layoutMarginsGuide.bottomAnchor, constant: 0),
            stackMagnitudes.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            stackMagnitudes.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            
            mapPointEarthquake.topAnchor.constraint(equalTo: stackInformation.bottomAnchor ,constant: 0),
            mapPointEarthquake.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            mapPointEarthquake.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            mapPointEarthquake.bottomAnchor.constraint(equalTo: stackMagnitudes.topAnchor, constant: -20),

        ])
    }
    

    override func layoutSubviews() {
        super.layoutSubviews()
        
        NSLayoutConstraint.activate([
            
            depthIcon.heightAnchor.constraint(equalToConstant: 38),
            depthIcon.widthAnchor.constraint(equalToConstant: 38),
            
            magnitudeIcon.heightAnchor.constraint(equalToConstant: 40),
            magnitudeIcon.widthAnchor.constraint(equalToConstant: 40),
            
        ])
        
        mapPointEarthquake.layer.cornerRadius = 12
        stackMagnitudes.layer.cornerRadius = 20
    }
    
    
    func setupEarthquakeDetails(feature : FeaturesModel){
        
        let magAprox = round(feature.properties.mag * 100) / 100
        let depthAprox = round(feature.geometry.coordinates[2] * 100) / 100

        self.earthquakeTitle.text = feature.properties.title
        self.earthquakePlace.text = feature.properties.place ?? "Desconocido"

        self.earthquakeMagnitude.text = "\(magAprox)"
        self.earthquakeDepth.text = "\(depthAprox) km"
    }
    
    func setMapRegion(_ region: MKCoordinateRegion) {
        self.mapPointEarthquake.setRegion(region, animated: false)
    }
    
    func setMapAnnotations(_ annotations: MKPointAnnotation) {
        self.mapPointEarthquake.addAnnotation(annotations)
    }
    
}
