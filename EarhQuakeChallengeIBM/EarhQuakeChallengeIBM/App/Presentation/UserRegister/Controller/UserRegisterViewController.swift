//
//  UserRegisterViewController.swift
//  EarhQuakeChallengeIBM
//
//  Created by FELIPE on 6/11/23.
//

import Foundation
import UIKit

class UserRegisterViewController: UIViewController {
    
    var userRegisterView : UserRegisterView {
        self.view as! UserRegisterView
    }
    
    //MARK: - coordinator
    weak var userLoginCoordinator : UserLoginCoordinator?
    
    //MARK: - metodos: crear peticion, crear alertas
    
    let userPersistentDataManager = UserPersistentDataManager()
    
   
    override func loadView() {
        super.loadView()
        view = UserRegisterView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userRegisterView.onAccept = { formData in

            self.validateForm(userData: formData)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        userRegisterView.nameTextField.becomeFirstResponder()
    }
    
}

extension UserRegisterViewController {
    
    //MARK: - validacion de campos que no sean vacios
    func validateForm(userData : UserRegisterModel) {
        
        if  userData.name  != "" && userData.lastname != "" && userData.email != "" && userData.password != "" {
            
            do{
                 
                try self.userPersistentDataManager.saveToKeyChains(model: userData)
                
                self.createAlert(title: "Registro exitoso", message: "") {
                    self.dismiss(animated: true)
                }
                
                
              }catch let error as UserPersistentDataErrors{
                  createAlert(title: "Lo sentimos", message: error.localizedDescriptionStorageError)
                  
              }catch {
                  createAlert(title: "Lo sentimos",
                              message: "Se produjo un error inesperado al intentar realizar el registro.")
              }
            
            
        }else if userData.name  == "" {
            createAlert(title: "Datos incompletos",
                        message: "Debes ingresar un Nombre para continuar")
            
        }else if userData.lastname  == "" {
            createAlert(title: "Datos incompletos",
                        message: "Debes ingresar un Apellido para continuar")
            
        }else if userData.email  == "" {
            createAlert(title: "Datos incompletos",
                        message: "Debes ingresar un Email para continuar")
            
        }else if userData.password  == "" {
            createAlert(title: "Datos incompletos",
                        message: "Debes ingresar una Contraseña para continuar")
        }
        
    }
    
    //MARK: - Creacion Alertas
   func createAlert(title: String, message: String, action: (() -> Void)? = nil ){
        
       AlertManager.alertToShow(context: self,
                                 title: title,
                                 message: message,
                                 buttonText: "Aceptar",
                                 action: {action?()}
        )
    }
    
    
    
}
