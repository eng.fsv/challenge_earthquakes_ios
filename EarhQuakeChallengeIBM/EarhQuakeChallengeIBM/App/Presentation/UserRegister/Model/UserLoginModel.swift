//
//  UserLoginModel.swift
//  EarhQuakeChallengeIBM
//
//  Created by FELIPE on 6/11/23.
//

import Foundation

struct UserLoginModel : Codable {
    
    let email : String
    let password : String
}
