//
//  UserRegisterModel.swift
//  EarhQuakeChallengeIBM
//
//  Created by FELIPE on 6/11/23.
//

import Foundation

struct UserRegisterModel : Codable {
    
    let name : String
    let lastname : String
    let email : String
    let password : String
}

