//
//  UserRegisterView.swift
//  EarhQuakeChallengeIBM
//
//  Created by FELIPE on 6/11/23.
//

import Foundation
import UIKit

class UserRegisterView: UIView {
    
    //MARK: - titulo modal
    let titleSection : UILabel = {
        let label = UILabel()
        label.text = "Crear Cuenta"
        label.textColor = UIColor(named: K.Colors.darkSecondary)
        label.font = UIFont.systemFont(ofSize: 24, weight: .medium)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    //MARK: - textFields
    //MARK: - textField name
    let nameTextField : UITextField = {
        let textField = UITextField()
        textField.attributedPlaceholder = NSAttributedString(
            string: "Nombre",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: K.Colors.darkSecondary)!]
        )
        textField.textColor = UIColor(named: K.Colors.darkPrimary)!
        let padding = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: 50))
        textField.leftView = padding
        textField.leftViewMode = .always
        textField.keyboardType = .default
        textField.tintColor = UIColor(named: K.Colors.darkPrimary)
        textField.autocorrectionType = .no
        return textField
    }()
    
    //MARK: - textField lastname
    let lastnameTextField : UITextField = {
        let textField = UITextField()
        textField.attributedPlaceholder = NSAttributedString(
            string: "Apellido",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: K.Colors.darkSecondary)!]
        )
        textField.textColor = UIColor(named: K.Colors.darkPrimary)!
        let padding = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: 50))
        textField.leftView = padding
        textField.leftViewMode = .always
        textField.keyboardType = .default
        textField.tintColor = UIColor(named: K.Colors.darkPrimary)
        textField.autocorrectionType = .no
        return textField
    }()
    
    //MARK: - textField email
    let emailTextField : UITextField = {
        let textField = UITextField()
        textField.attributedPlaceholder = NSAttributedString(
            string: "Email",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: K.Colors.darkSecondary)!]
        )
        textField.textColor = UIColor(named: K.Colors.darkPrimary)!
        let padding = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: 50))
        textField.leftView = padding
        textField.leftViewMode = .always
        textField.keyboardType = .emailAddress
        textField.tintColor = UIColor(named: K.Colors.darkPrimary)
        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none
        return textField
    }()
    
    //MARK: - textField password
    let passwordTextField : UITextField = {
        let textField = UITextField()
        textField.attributedPlaceholder = NSAttributedString(
            string: "Contraseña",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: K.Colors.darkSecondary)!]
        )
        textField.textColor = UIColor(named: K.Colors.darkPrimary)!
        let padding = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: 50))
        textField.leftView = padding
        textField.leftViewMode = .always
        textField.keyboardType = .default
        textField.isSecureTextEntry = true
        textField.tintColor = UIColor(named: K.Colors.darkPrimary)
        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none
        return textField
    }()
    
    //MARK: - buttonAccept
    var onAccept : ((UserRegisterModel) -> Void)?
    
    var buttonAccept = UIButton()
    
    var configurationButtonAccept : UIButton.Configuration = {
        var configuration = UIButton.Configuration.filled()
        configuration.cornerStyle = .capsule
        configuration.baseForegroundColor = UIColor(named: K.Colors.lightPrimary)!
        configuration.baseBackgroundColor = UIColor(named: K.Colors.darkSecondary)!
        configuration.title = "Aceptar"
        return configuration
    }()
    
    
    //Stack form
    let stackForm : UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.spacing = 25
        stack.alignment = .fill
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    
    @objc func buttonAcceptTapped(){
        
        guard let name = nameTextField.text else {return}
        guard let lastname = lastnameTextField.text else {return}
        guard let email = emailTextField.text else {return}
        guard let password = passwordTextField.text else {return}
        
        let newUserRegisterModel = UserRegisterModel(
            name: name,
            lastname: lastname,
            email: email,
            password: password
        )
        
        onAccept?(newUserRegisterModel)
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupRegisterSheet()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func setupRegisterSheet(){
        
        backgroundColor = UIColor(named: K.Colors.lightPrimary)
        
        buttonAccept = UIButton(configuration: configurationButtonAccept)
        buttonAccept.addTarget(self, action: #selector(buttonAcceptTapped), for: .touchUpInside)
        
        buttonAccept.translatesAutoresizingMaskIntoConstraints = false
        
        stackForm.addArrangedSubview(nameTextField)
        stackForm.addArrangedSubview(lastnameTextField)
        stackForm.addArrangedSubview(emailTextField)
        stackForm.addArrangedSubview(passwordTextField)
        stackForm.addArrangedSubview(buttonAccept)
        
        addSubview(titleSection)
        addSubview(stackForm)
        
        NSLayoutConstraint.activate([
            
            titleSection.centerXAnchor.constraint(equalTo: centerXAnchor),
            titleSection.topAnchor.constraint(equalTo: topAnchor, constant: 30),
            
            stackForm.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 30),
            stackForm.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -30),
            stackForm.topAnchor.constraint(equalTo: titleSection.bottomAnchor, constant: 30),
            stackForm.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.4)
            
        ])
        
    }
    
}
