//
//  EarthquakeListView.swift
//  EarhQuakeChallengeIBM
//
//  Created by FELIPE on 6/11/23.
//

import Foundation
import UIKit

class EarthquakeListView: UIView {
    
    //MARK: - vista tabla
    let earthquakeTableView : UITableView = {
        let tableview = UITableView()
        tableview.translatesAutoresizingMaskIntoConstraints = false
        tableview.register(EarthquakeListViewCell.self, forCellReuseIdentifier: "EarthquakeListViewCell")
        return tableview
    }()
    
    //MARK: - selectores superiores de fecha
    //MARK: - container selectores superiores de fecha
    let pickerDateStack : UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fillProportionally
        stack.alignment = .center
        stack.spacing = 20
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.layoutMargins = UIEdgeInsets(top: 5, left: 15, bottom: 10, right: 15)
        stack.isLayoutMarginsRelativeArrangement = true
        stack.backgroundColor = UIColor(named: K.Colors.bottomMapTab)
        return stack
    }()
    
    //MARK: - contenedor fecha inicio
    let startDateStack : UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.alignment = .center
        stack.spacing = 10
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    //MARK: - titulo fecha inicio
    let startDateLabel : UILabel = {
        let label = UILabel()
        label.textColor = UIColor.label
        label.text = "Fecha Inicial"
        label.textAlignment = .center
        return label
    }()
    
    //MARK: - selector fecha inicial
    let startDatePicker : UIDatePicker = {
        let picker = UIDatePicker()
        picker.translatesAutoresizingMaskIntoConstraints = false
        picker.datePickerMode = .date
        picker.maximumDate = Date()
        return picker
    }()
    
    //MARK: - contenedor fecha fin
    let endDateStack : UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.alignment = .center
        stack.spacing = 10
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    //MARK: - titulo fecha fin
    let endDateLabel : UILabel = {
        let label = UILabel()
        label.textColor = UIColor.label
        label.text = "Fecha Final"
        label.textAlignment = .center
        return label
    }()
    
    //MARK: - selector fecha fin
    let endDatePicker : UIDatePicker = {
        let picker = UIDatePicker()
        picker.translatesAutoresizingMaskIntoConstraints = false
        picker.datePickerMode = .date
        picker.maximumDate = Date()
        return picker
    }()
    
    //MARK: - button search
    var buttonSearch : UIButton!
    
    //MARK: - configuracion button search
    var configurationButtonSearch: UIButton.Configuration = {
        var configuration = UIButton.Configuration.filled()
        configuration.cornerStyle = .capsule
        configuration.baseForegroundColor = UIColor(named: K.Colors.bottomMapTab)
        configuration.baseBackgroundColor = UIColor.accent
        configuration.imagePlacement = .trailing
        configuration.imagePadding = 10
        return configuration
    }()
    
    //MARK: - closur devuelve array de fechas
    var onSearch : (([Date]) -> Void)?

    //MARK: - funcion activada por button search
    @objc func buttonSearchTapped(){
        let startDate = startDatePicker.date
        let endDate   = endDatePicker.date
        onSearch?([startDate, endDate])
    }

    //MARK: - inits
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupTable()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - setups
    func setupTable(){
        
        backgroundColor = UIColor.systemBackground
        
        buttonSearch = UIButton(configuration: configurationButtonSearch)
        buttonSearch.addTarget(self, action: #selector(buttonSearchTapped), for: .touchUpInside)
        buttonSearch.translatesAutoresizingMaskIntoConstraints = false
        buttonSearch.setImage(UIImage(systemName: "magnifyingglass"), for: .normal)
        
        //MARK: - adicion vistas
        startDateStack.addArrangedSubview(startDateLabel)
        startDateStack.addArrangedSubview(startDatePicker)
        
        endDateStack.addArrangedSubview(endDateLabel)
        endDateStack.addArrangedSubview(endDatePicker)
        
        pickerDateStack.addArrangedSubview(startDateStack)
        pickerDateStack.addArrangedSubview(endDateStack)
        pickerDateStack.addArrangedSubview(buttonSearch)
        
        addSubview(pickerDateStack)
        addSubview(earthquakeTableView)
 
        
        //MARK: - adicion Constraints
        NSLayoutConstraint.activate([
            
            buttonSearch.widthAnchor.constraint(equalToConstant: 40),
            buttonSearch.heightAnchor.constraint(equalToConstant: 40),
            
            pickerDateStack.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor, constant: 5),
            pickerDateStack.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15),
            pickerDateStack.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15),
            
            earthquakeTableView.topAnchor.constraint(equalTo: pickerDateStack.bottomAnchor, constant: 10),
            earthquakeTableView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            earthquakeTableView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            earthquakeTableView.bottomAnchor.constraint(equalTo: layoutMarginsGuide.bottomAnchor),
  
        ])
        
        pickerDateStack.layer.cornerRadius = 20
        
    }
}

