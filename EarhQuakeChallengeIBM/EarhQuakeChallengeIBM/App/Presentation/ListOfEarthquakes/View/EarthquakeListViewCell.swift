//
//  EarthquakeListViewCell.swift
//  EarhQuakeChallengeIBM
//
//  Created by FELIPE on 6/11/23.
//

import Foundation
import UIKit

class EarthquakeListViewCell: UITableViewCell {
    
    //MARK: - labels cell
    let earthquakePlace : UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .left
        label.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        label.textColor = UIColor(named: K.Colors.bottomIconTab)
        return label
    }()
    
    let earthquakeTitle : UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .left
        label.textColor = UIColor.label
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont.systemFont(ofSize: 16, weight: .light)
        return label
    }()
    
    
    let earthquakeMagnitudes : UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .left
        label.textColor = UIColor.label
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont.systemFont(ofSize: 16, weight: .light)
        return label
    }()
 
    
    //MARK: - container cell
    let cellStack : UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fill
        stack.alignment = .fill
        stack.spacing = 5
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()

    //MARK: - inits
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCell()
    }
    
    //MARK: - setups
    func setupCell(){
        
        cellStack.addArrangedSubview(earthquakePlace)
        cellStack.addArrangedSubview(earthquakeTitle)
        cellStack.addArrangedSubview(earthquakeMagnitudes)
        
        addSubview(cellStack)

        NSLayoutConstraint.activate([
             cellStack.topAnchor.constraint(equalTo: topAnchor, constant: 15),
             cellStack.bottomAnchor.constraint(equalTo: bottomAnchor,constant: -15),
             cellStack.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
             cellStack.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -35),

        ])

    }
  
    
    func setupEarthquake(_ feature : FeaturesModel){
        
        self.earthquakePlace.text = feature.properties.place ?? "Desconocido"
        self.earthquakeTitle.text = feature.properties.title
        
        let magAprox = round(feature.properties.mag * 100) / 100
        let depthAprox = round(feature.geometry.coordinates[2] * 100) / 100
        
        self.earthquakeMagnitudes.text = "Magnitud: \(magAprox) - Profundidad: \(depthAprox) Km"
    
    }
    
   
    
}
