//
//  EarthquakesListViewController.swift
//  EarhQuakeChallengeIBM
//
//  Created by FELIPE on 6/11/23.
//

import Foundation

import UIKit
import MapKit


class EarthquakesListViewController: UIViewController {
    
    //MARK: - metodos: crear peticion, crear alertas
    let client = ListOfEathquakesAPIClient()
    
    //MARK: - instancias:  Vista, Delegado tabla y dataSource tabla
    var earthquakeListView : EarthquakeListView { self.view as! EarthquakeListView }
    private var tableviewDataSource :   EarthquakesListViewDataSource?
    private var tableviewDelegate   :   EarthquakesListViewDelegate?
    
    //MARK: - coordinator
    weak var coordinator : MainEarthquakeListCoordinator?
    
    //MARK: - constante: calendario, variable: fecha inicio y fin seleccionadas
    let calendar = Calendar.current
    var startDateSelected = Date()
    var endDateSelected = Date()
    
    //MARK: - variables: array para tabla, contador de paginas, bandera para detener peticiones
    var listEarthquakes : [FeaturesModel] = []
    var currentPage : Int = 1
    var continueLoading : Bool = true
    
    //MARK: - foramateador a ISO8601 de fechas del datePicker
    let dateFormatter : ISO8601DateFormatter = {
        let dateFormat = ISO8601DateFormatter()
        dateFormat.formatOptions = [.withInternetDateTime]
        return dateFormat
    }()
    
    //MARK: - metodo inicial, asignacion de vista, y seteo de tabla
    override func loadView() {
        super.loadView()
        view = EarthquakeListView()
        tableviewDataSource = EarthquakesListViewDataSource(tableview: earthquakeListView.earthquakeTableView)
        tableviewDelegate = EarthquakesListViewDelegate()
        earthquakeListView.earthquakeTableView.dataSource = tableviewDataSource
        earthquakeListView.earthquakeTableView.delegate = tableviewDelegate
    }
    
    //MARK: - metodo secundario
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Terremotos"
        
        //MARK: - LogoutButton
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            title: "Cerrar Sesión",
            style: .plain,
            target: self,
            action: #selector(logoutButtonTapped)
        )
        
        //MARK: - tap search Button
        earthquakeListView.onSearch = { dates in
            
            //MARK: - asignacion fechas seleccionadas, restablecimiento de variables
            self.startDateSelected = dates[0]
            self.endDateSelected = dates[1]
            self.listEarthquakes.removeAll()
            self.currentPage = 1
            self.continueLoading = true
            
            //MARK: - validacion rango fecha
            if dates[1] < dates[0] {
                
                self.createAlert(title: "Rango invalido",
                            message: "Verifica que la fecha inicial sea menor a la fecha final."
                )
                
            }else{
                
                Task{
                    do{
                        //MARK: - fetch de pagina 1 en el rango
                        let earthquakes = try await self.getEarthquakes(start: dates[0],
                                                                        end: dates[1],
                                                                        page: self.currentPage)
                        
                        guard let earthquakes = earthquakes else {return}
                        
                        //MARK: - carga de datos en variable y en tabla
                        self.listEarthquakes  = earthquakes.features
                        self.tableviewDataSource?.setEarthquakes(features: earthquakes.features)
                        
                        //MARK: - actualizacion de tabla
                        DispatchQueue.main.async {
                            self.earthquakeListView.earthquakeTableView.reloadData()
                            self.earthquakeListView.earthquakeTableView.scrollToRow(at: IndexPath(row: 0, section: 0),
                                                                                    at: .top, animated: true)
                        }
                        
                    }catch {
                        //MARK: - manejo de error
                        self.createAlert(title: "Lo sentimos", message: "La aplicacion se ha detenido")
                    }
                }
            }
        }
        
        
        //MARK: - carga inicial de la vista
        Task {
            
            do{
                //MARK: - fetch inicial de ultimos terremotos en fecha actual
                let earthquakes = try await getEarthquakes(start: nil, end: endDateSelected, page: 1)
                guard let earthquakes = earthquakes else {return}
                
                //MARK: - carga de datos en variable y en tabla
                self.listEarthquakes  = earthquakes.features
                tableviewDataSource?.setEarthquakes(features: earthquakes.features)
                
                //MARK: - actualizacion de tabla
                DispatchQueue.main.async {
                    self.earthquakeListView.earthquakeTableView.reloadData()
                    self.earthquakeListView.earthquakeTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                }
                
            }catch {
                createAlert(title: "Lo sentimos", message: "La aplicacion se ha detenido")
            }
        }
        
        
        //MARK: - Manejo de paginacion
        tableviewDelegate?.willReload = {
            
            //MARK: - bandera que detiene la carga si respuesta llega vacia
            if self.continueLoading {
                
                //MARK: - calculo de offset
                let page = self.currentPage * 10
                
                Task{
                    do{
                        
                        var currentBatchItems : [FeaturesModel] = self.listEarthquakes
                        
                        //MARK: - elimina el ultimo elemento duplicado, por offset
                        if self.currentPage == 1 {
                            currentBatchItems.removeLast()
                        }
                        
                        //MARK: - fetch de pagina siguiente
                        let earthquakes = try await self.getEarthquakes(start: self.startDateSelected,
                                                                        end: self.endDateSelected, page: page)
                        
                        guard let newEarthquakes = earthquakes else {return}
                        
                        //MARK: - alerta: no hay mas resultados
                        if newEarthquakes.features.count == 0 {
                            self.continueLoading = false
                            self.createAlert(title: "Sin Resultados",
                                             message: "No se encontraron mas Terremotos en el rango actual, intenta con un rango nuevo")
                        }
                        
                        //MARK: - adicion de nuevos items
                        currentBatchItems.append(contentsOf: newEarthquakes.features)
                        
                        //MARK: - carga de datos en variable y en tabla
                        self.listEarthquakes = currentBatchItems
                        self.tableviewDataSource?.setEarthquakes(features: currentBatchItems)
                        
                        //MARK: - incremento de pagina
                        self.currentPage += 1
                        
                        //MARK: - recarga de tabla
                        DispatchQueue.main.async {
                            self.earthquakeListView.earthquakeTableView.reloadData()
                        }
                        
                        
                    }catch let error{
                        self.createAlert(title: "Lo sentimos", message: "La aplicacion se ha detenido. \(error)")
                    }
                    
                    
                }
            }
        }
        
        //MARK: - extract index cell and navigate
        
        tableviewDelegate?.didTapCell = { index in
            
            //MARK: - verifica que DataSource no sea igual a nil
            guard let dataSource = self.tableviewDataSource else {return}
            
            //MARK: - carga en variable el feature actual
            let feature = dataSource.features[index]
            
            //MARK: - seteo de coordinator
            
            self.coordinator?.showDetailsForItem(feature: feature)
            
            
        }
    }
    
}



extension EarthquakesListViewController {
    
    //MARK: - funcion para logout
    @objc func logoutButtonTapped(){
        coordinator?.userTappedLogout()
    }
    
    //MARK: - funcion para crear peticion
    func getEarthquakes(start: Date?, end: Date, page: Int) async throws -> EarthquakeModel? {
        
        do {
            
            //MARK: - Creacion de url con parametros
            let urlCreated = loadItemsOnRange(startDate: start, endDate: end, page: page)
            
            //MARK: - asignacion de items del response
            let earthquakesResponse = try await client.getListOfEarthquakes(urlComponents: urlCreated )
            
            return earthquakesResponse
            
        }catch let error as ListOfEathquakesAPIError {
            //MARK: - manejo de response si no se tiene exito
            createAlert(title: "Lo sentimos",
                        message: error.localizedDescriptionAPIError)
        }
        
        return nil
    }
    
    
    //MARK: - funcion para crear url
    func loadItemsOnRange(startDate: Date?, endDate: Date, page: Int) -> URLComponents {
        
        //MARK: - Array de URLQueryItem
        var queryItems : [URLQueryItem] = []
        
        //MARK: - obtencion del final del dia de endDate
        let endOfDay = calendar.date(bySettingHour: 23, minute: 59, second: 59, of: endDate)
        
        //MARK: - string del final del dia de endDate
        let endDateString = dateFormatter.string(from: endOfDay!)
        
        //MARK: - string del numero de pagina que es el offset
        let page = String(page)
        
        //MARK: - crea url con los parametros que como minimo se requieren
        var urlCom = URLComponents(string: K.Endpoint.earthquakeEndpoint)!
        
        //MARK: - asignacion de parametros
        queryItems.append(URLQueryItem(name: "format",   value: "geojson"))
        queryItems.append( URLQueryItem(name: "orderby",  value: "time"))
        queryItems.append(URLQueryItem(name: "limit",    value: "10"))
        queryItems.append(URLQueryItem(name: "offset",  value: page))
        queryItems.append(URLQueryItem(name: "endtime",  value: endDateString))
        
        //MARK: - asignacion de fecha de inicio opcional si se requiere un rango
        if let startDate = startDate {
            let startOfDay = calendar.startOfDay(for: startDate)
            let startDateString = dateFormatter.string(from: startOfDay)
            queryItems.append( URLQueryItem(name: "starttime",value: startDateString))
        }
        
        urlCom.queryItems = queryItems
        
        return urlCom
    }
    
    //MARK: - funcion para crear alertas
    func createAlert(title: String, message: String ){
        
        AlertManager.alertToShow(context: self,
                                 title: title,
                                 message: message,
                                 buttonText: "Aceptar"
        )
    }
    
}
