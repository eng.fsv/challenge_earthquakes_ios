//
//  EarthquakesListViewDelegate.swift
//  EarhQuakeChallengeIBM
//
//  Created by FELIPE on 6/11/23.
//

import Foundation
import UIKit

//MARK: - Manejo de delegado de Tabla

class EarthquakesListViewDelegate : NSObject ,UITableViewDelegate {
  
    //MARK: - variable closure, retornara el indice de la celda seleccionada
    var didTapCell : ((Int) -> Void)?
    
    //MARK: - variable closure, se activara cuando la tabla casi llegue al final
    var willReload : (()-> Void)?
    
    //MARK: - se activa al tocar una celda
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        didTapCell?(indexPath.row)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK: - se activa al casi final de la tabla
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

      if indexPath.row == tableView.numberOfRows(inSection: 0) - 2{
            willReload?()
        }
    }
 
    
}
