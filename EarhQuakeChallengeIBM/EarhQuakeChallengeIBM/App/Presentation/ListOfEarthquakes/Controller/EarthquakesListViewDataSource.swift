//
//  EarthquakesListViewDataSource.swift
//  EarhQuakeChallengeIBM
//
//  Created by FELIPE on 6/11/23.
//

import Foundation

import UIKit

//MARK: - Manejo de datasource Tabla
class EarthquakesListViewDataSource : NSObject ,UITableViewDataSource {
    
    private let tableview : UITableView
    
    var features : [FeaturesModel] = []
  
    
    //MARK: - metodo para cambiar features
    func setEarthquakes (features: [FeaturesModel]){
        self.features = features
    }
   
    
    //MARK: - Inicializador de clase
    init(tableview: UITableView, features: [FeaturesModel] = []) {
        self.tableview = tableview
        self.features = features
    }

    //MARK: - seteo de cantidad de filas
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        features.count
    }
    
    //MARK: - seteo de celdas
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EarthquakeListViewCell", for: indexPath) as! EarthquakeListViewCell
        
        //MARK: - asignacion de feature a celda en fila
        let feature = features[indexPath.row]
        cell.setupEarthquake(feature)
       
        //MARK: - accesory personalizado
        let icon = UIImage(systemName: "chevron.right")?.withTintColor(UIColor(named: K.Colors.bottomIconTab)!)
        let accessory  = UIImageView(frame:CGRect(x:0, y:0, width:(icon?.size.width)!, height:(icon?.size.height)!))
        accessory.image = icon
        cell.accessoryView = accessory

        return cell
    }

}
