//
//  FeaturesModel.swift
//  EarhQuakeChallengeIBM
//
//  Created by FELIPE on 6/11/23.
//

import Foundation

struct FeaturesModel: Decodable {

    let properties : PropertiesModel
    let geometry : GeometryModel
}

