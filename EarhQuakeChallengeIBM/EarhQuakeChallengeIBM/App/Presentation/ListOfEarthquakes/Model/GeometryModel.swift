//
//  GeometryModel.swift
//  EarhQuakeChallengeIBM
//
//  Created by FELIPE on 6/11/23.
//

import Foundation

struct GeometryModel : Decodable {
    
    let coordinates : [Double]
}
