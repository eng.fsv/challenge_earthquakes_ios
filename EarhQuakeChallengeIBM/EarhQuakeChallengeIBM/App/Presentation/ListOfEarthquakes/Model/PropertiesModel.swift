//
//  PropertiesModel.swift
//  EarhQuakeChallengeIBM
//
//  Created by FELIPE on 6/11/23.
//

import Foundation

struct PropertiesModel : Decodable {
    
    let place : String?
    let title : String
    let mag : Float
}
