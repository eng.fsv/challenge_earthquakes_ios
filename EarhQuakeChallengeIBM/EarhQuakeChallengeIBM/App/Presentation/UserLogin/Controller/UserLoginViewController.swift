//
//  UserLoginViewController.swift
//  EarhQuakeChallengeIBM
//
//  Created by FELIPE on 6/11/23.
//

import Foundation
import UIKit

class UserLoginViewController: UIViewController {
    
    var userLoginView : UserLoginView { self.view as! UserLoginView}
    
    //MARK: - coordinator
    weak var coordinator : UserLoginCoordinator?
    
    //MARK: - metodos: crear peticion, crear alertas
    let userPersistentDataManager = UserPersistentDataManager()
    
    
    override func loadView() {
        super.loadView()
        view = UserLoginView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userLoginView.onSignIn = {
            self.coordinator?.showRegisterModal()
        }
        
        userLoginView.onAccept = { loginData in
            self.validateUserLogin(loginData : loginData)
        }
    }
    
}


extension UserLoginViewController {
    
    //MARK: - metodos: crear peticion, crear alertas
    func createAlert(title: String, message: String, action: (() -> Void)? = nil ){
        
        AlertManager.alertToShow(context: self,
                                 title: title,
                                 message: message,
                                 buttonText: "Aceptar",
                                 action: {action?()}
        )
    }
    
    //MARK: - vvalida que los datos sean correctos, Keychain
    func validateUserLogin(loginData : UserLoginModel){
        
        do{
            let readedKC = try userPersistentDataManager.readFromKeyChains(email: loginData.email)
            
            if readedKC?.password == loginData.password {
                
                self.coordinator?.succesfullLogin()
                
            }else{
                createAlert(title: "Contrasena inválida", message: "La contraseña que ingresaste es incorrecta. Revisa e ingresa de nuevo tu contraseña")
            }
            
        }catch let error as UserPersistentDataErrors{
            createAlert(title: "Lo sentimos",
                        message: error.localizedDescriptionStorageError)
            
        }catch {
            createAlert(title: "Lo sentimos",
                        message: "Se produjo un error inesperado al intentar recuperar los datos.")
        }
    }
    
}
