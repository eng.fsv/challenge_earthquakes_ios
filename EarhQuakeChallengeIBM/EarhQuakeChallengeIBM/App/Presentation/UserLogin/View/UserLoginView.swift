//
//  UserLoginView.swift
//  EarhQuakeChallengeIBM
//
//  Created by FELIPE on 6/11/23.
//

import Foundation
import UIKit

class UserLoginView: UIView {
    
    //MARK: - container
    let UserLoginBackground : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(named: K.Colors.darkPrimary)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
 
    //MARK: - top icon stack
    let iconStack : UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .center
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    let iconImage : UIImageView = {
        let image = UIImageView()
        image.image = UIImage(systemName: "globe.americas")
        image.tintColor = UIColor(named: K.Colors.lightPrimary)
        image.tintAdjustmentMode = .normal
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    let iconLabel : UILabel = {
        let label = UILabel()
        label.textColor = UIColor(named: K.Colors.lightPrimary)
        label.text = "Earthquake"
        label.font = UIFont.systemFont(ofSize: 34, weight: .medium)
        return label
    }()
    

    //MARK: - textFields Stacks
    
    let emailTextField : UITextField = {
        let textField = UITextField()
        textField.backgroundColor = UIColor.clear
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = UIColor(named: K.Colors.lightPrimary)
        
        textField.attributedPlaceholder = NSAttributedString(
            string: "Email",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: K.Colors.lightSecondary)!]
        )
        let padding = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: 50))
        textField.leftView = padding
        textField.leftViewMode = .always
        textField.keyboardType = .emailAddress
        textField.autocapitalizationType = .none
        textField.autocorrectionType = .no
        
        return textField
    }()
    
    let passwordTextField : UITextField = {
        let textField = UITextField()
        textField.backgroundColor = UIColor.clear
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = UIColor(named: K.Colors.lightPrimary)

        textField.attributedPlaceholder = NSAttributedString(
            string: "Contraseña",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: K.Colors.lightSecondary)!]
        )
        let padding = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: 50))
        textField.leftView = padding
        textField.leftViewMode = .always
        textField.keyboardType = .emailAddress
        
        textField.isSecureTextEntry = true
        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none
        return textField
    }()
    
    //MARK: - button login
    var buttonLogin = UIButton()
    
    var configurationButton : UIButton.Configuration = {
        var configuration = UIButton.Configuration.filled()
        configuration.cornerStyle = .capsule
        configuration.baseForegroundColor = UIColor(named: K.Colors.darkPrimary)
        configuration.baseBackgroundColor = UIColor(named: K.Colors.lightPrimary)
        configuration.imagePlacement = .trailing
        configuration.imagePadding = 10
        return configuration
    }()
    
    var onAccept : ((UserLoginModel) -> Void)?
    
    @objc func buttonLoginTapped(){
        
        guard let email = emailTextField.text, email != "" else { return }
        guard let password =  passwordTextField.text, password != "" else { return }
        
        let newUserLoginModel = UserLoginModel(email: email,password: password)
        
        onAccept?(newUserLoginModel)
        
    }
    
    //MARK: - button bottom stack
    let signInStack : UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.alignment = .center
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    let signInLabel : UILabel = {
        let label = UILabel()
        label.textColor = UIColor(named: K.Colors.lightPrimary)
        label.text = "¿Aún no tienes una cuenta?"
        label.font = UIFont.systemFont(ofSize: 16, weight: .thin)
        return label
    }()
    
    var buttonSignIn = UIButton()
    
    var configurationButtonSignIn : UIButton.Configuration = {
        var configuration = UIButton.Configuration.plain()
        configuration.baseForegroundColor = UIColor(named: K.Colors.lightPrimary)
        configuration.baseBackgroundColor = UIColor.white
        return configuration
    }()
    
    var onSignIn : (() -> Void)?
    
    @objc func buttonSignInTapped(){
        onSignIn?()
    }
    
    //MARK: - inits
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLoginView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupLoginView(){

        iconStack.addArrangedSubview(iconImage)
        iconStack.addArrangedSubview(iconLabel)

        buttonLogin = UIButton(configuration: configurationButton)
        buttonLogin.addTarget(self, action: #selector(buttonLoginTapped), for: .touchUpInside)
        buttonLogin.setTitle("Iniciar Sesión", for: .normal)
        buttonLogin.configuration?.image = UIImage(systemName: "rectangle.portrait.and.arrow.right")
        buttonLogin.translatesAutoresizingMaskIntoConstraints = false
        
        buttonSignIn = UIButton(configuration: configurationButtonSignIn)
        buttonSignIn.addTarget(self, action: #selector(buttonSignInTapped), for: .touchUpInside)
        buttonSignIn.setTitle("Registrarme", for: .normal)
        
        addSubview(UserLoginBackground)
        addSubview(iconStack)
        
        addSubview(emailTextField)
        addSubview(passwordTextField)
        
        addSubview(buttonLogin)
        
        signInStack.addArrangedSubview(signInLabel)
        signInStack.addArrangedSubview(buttonSignIn)
        addSubview(signInStack)
        
        NSLayoutConstraint.activate([
            
            UserLoginBackground.topAnchor.constraint(equalTo: topAnchor),
            UserLoginBackground.leadingAnchor.constraint(equalTo: leadingAnchor),
            UserLoginBackground.trailingAnchor.constraint(equalTo: trailingAnchor),
            UserLoginBackground.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            iconStack.centerXAnchor.constraint(equalTo: centerXAnchor),
            iconStack.topAnchor.constraint(equalTo: topAnchor, constant: 100),
            
            iconImage.widthAnchor.constraint(equalToConstant: 100),
            iconImage.heightAnchor.constraint(equalToConstant: 100),
            
        ])
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        NSLayoutConstraint.activate([
            
            emailTextField.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.85),
            emailTextField.centerXAnchor.constraint(equalTo: centerXAnchor),
            emailTextField.topAnchor.constraint(equalTo: iconStack.bottomAnchor, constant: 100),
            emailTextField.heightAnchor.constraint(equalToConstant: 50),
            
            passwordTextField.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.85),
            passwordTextField.centerXAnchor.constraint(equalTo: centerXAnchor),
            passwordTextField.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: 25),
            passwordTextField.heightAnchor.constraint(equalToConstant: 50),
            
            buttonLogin.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.85),
            buttonLogin.centerXAnchor.constraint(equalTo: centerXAnchor),
            buttonLogin.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 25),
            buttonLogin.heightAnchor.constraint(equalToConstant: 50),
            
            signInStack.centerXAnchor.constraint(equalTo: centerXAnchor),
            signInStack.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -40),
 
        ])
    }
}
