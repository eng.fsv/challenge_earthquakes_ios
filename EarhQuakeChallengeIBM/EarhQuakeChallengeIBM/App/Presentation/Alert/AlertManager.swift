//
//  AlertManager.swift
//  EarhQuakeChallengeIBM
//
//  Created by FELIPE on 6/11/23.
//

import Foundation
import UIKit

//Funcion para mostrar alertas

class AlertManager {
    
    static func alertToShow(
        context: UIViewController,
        title: String?,
        message: String?,
        buttonText: String?,
        action: (() -> Void)? = nil,
        cancelButton: String? = nil,
        cancelButtonAction: (() -> Void)? = nil
    ){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: buttonText, style: .default) { _ in
            action?()
        }
        
        defaultAction.setValue(UIColor.accent, forKey: "titleTextColor")
        
        alert.addAction(defaultAction)
        
        if let _ = cancelButton {
            let secondaryAction = UIAlertAction(title: cancelButton, style: .default) { _ in
                cancelButtonAction?()
            }
            alert.addAction(secondaryAction)
        }
        
        context.present(alert, animated: true, completion: nil)
    }
    
}
