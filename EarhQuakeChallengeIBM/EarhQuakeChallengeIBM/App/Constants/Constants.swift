//
//  Constants.swift
//  EarhQuakeChallengeIBM
//
//  Created by FELIPE on 6/11/23.
//

import Foundation

//MARK: - constantes
struct K {
    
    //MARK: - version app
    struct InfoApp {
        static let name     = "Earthquake"
        static let version  = "1.0"
    }
    
    //MARK: - colores en Assets
    struct Colors {
        
        static let darkPrimary      = "darkPrimary"
        static let darkSecondary    = "darkSecondary"
        static let lightPrimary     = "lightPrimary"
        static let lightSecondary   = "lightSecondary"
        static let bottomMapTab     = "bottomMapTab"
        static let bottomIconTab    = "bottomIconTab"
    }
   
    //MARK: - Endpoint
    struct Endpoint {
        static let earthquakeEndpoint = "https://earthquake.usgs.gov/fdsnws/event/1/query"
    }
    
    //MARK: - Defaults
    struct Defaults{
        static let userDidLogin = "UserDidLogin"
    }
    
}
