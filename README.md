# Challenge Integración con api pública API EARTHQUAKES, junto a un login de usuario natívo.

## Link del Repositorio del Proyecto https://gitlab.com/eng.fsv/challenge_earthquakes_ios.git

## Link con Entregables https://drive.google.com/drive/folders/1Ue46gavrq5ZIIM-Fs1jGFNc7qCKD-615?usp=sharing

Esta aplicación se ha desarrollado en UIKit iOS, con la finalidad de resolver una prueba técnica haciendo uso de la API de Earthquakes y la implementación de diversas técnicas de desarrollo.

## Características

- Patron de arquitectura: Clean Architecture
- Patron de arquitectura: Model View Controller
- Patron de diseño: Coordinator
- Persistencia de datos: KeyChains

## Descripción del Proyecto

La aplicación de Visualización de Terremotos permite a los usuarios ver información sobre terremotos recientes y en un rango específico de la API de Earthquakes. La aplicación consta de las siguientes vistas:

- **Login:** Permite a los usuarios iniciar sesión en la aplicación. Los datos de inicio de sesión se almacenan localmente.

- **Registro:** Permite a los usuarios registrarse en la aplicación para acceder a las funcionalides.

- **Main:** Muestra una lista de terremotos recientes recuperados de la API. Los usuarios pueden seleccionar un terremoto para obtener más detalles. Los usuarios pueden establecer un rango de búsqueda.

- **Details:** Muestra información detallada sobre un terremoto seleccionado, como su magnitud, profundidad y ubicación.

## API Reference (https://earthquake.usgs.gov/fdsnws/event/1)

#### Get Últimos 10 ordenados desde 'endtime'

```http
  GET /query?format=geojson&orderby=time&limit=10&offset=1&endtime=2023-11-05T04:59:59Z
```

| Parámetro         | Type              | Description                |
| :--------         | :-------------    | :------------------------- |
| `query?format`    | `geojson`         | Especifica el dato de salida| 
| `orderby`         | `time`            | orden por origen tiempo descendente| 
| `limit`           | `10`              | Limite los resultados al número especificado de eventos.                    | 
| `offset`          | `1`         |Devuelve resultados comenzando en el recuento de eventos.                       | 
| `endtime`           | `2023-11-05T04:59:59Z`              |Formato Fecha/Hora ISO8601|
  


## Screenshots Login

![App Screenshot Login](https://gitlab.com/eng.fsv/challenge_earthquakes_ios/-/raw/main/Screenshots/Login.png?ref_type=heads)

## Screenshots Main y Details Dark Mode

![App Screenshot Login](https://gitlab.com/eng.fsv/challenge_earthquakes_ios/-/raw/main/Screenshots/Main.png?ref_type=heads)

## Screenshots Main y Details Light Mode

![App Screenshot Login](https://gitlab.com/eng.fsv/challenge_earthquakes_ios/-/raw/main/Screenshots/MainLight.png?ref_type=heads)

## Autor

- [@eng.fsv](https://gitlab.com/eng.fsv)
